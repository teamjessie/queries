SELECT 
m.SubscriberKey,
m.EmailAddress,
ml.Area__c AS MostBookedLocation
FROM Master_Contacts m
INNER JOIN (
 SELECT TOP(100) PERCENT Customer_ID__c, area__c as Area__c, count(area__c) as location_count,
 ROW_NUMBER() OVER(PARTITION BY Customer_ID__c ORDER BY count(area__c) DESC) as rc
 FROM Test_Booking
 GROUP BY Customer_ID__c, area__c
 ORDER BY count(area__c) DESC
) as ml
ON ml.Customer_ID__c = m.SubscriberKey
AND ml.rc = 1