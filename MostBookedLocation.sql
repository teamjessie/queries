SELECT
m.SubscriberKey,
m.EmailAddress,
ml.area__c AS MostBookedLocation

FROM MasterContact m
INNER JOIN
    (SELECT lc.Customer_ID__c, MAX(lc.location_count) AS toplocationcount
    FROM
        (SELECT
        Customer_ID__c,
        area__c,
        count(area__c) as location_count
        FROM booking__c
        GROUP BY Customer_ID__c, area__c) lc
    GROUP BY lc.Customer_ID__c) tc
ON m.SubscriberKey = tc.Customer_ID__c

INNER JOIN
    (SELECT
        Customer_ID__c,
        area__c,
        count(area__c) as location_count
        FROM booking__c
        GROUP BY Customer_ID__c, area__c) ml
ON m.SubscriberKey = ml.Customer_ID__c
AND tc.toplocationcount = ml.location_count