SELECT
m.SubscriberKey,
m.EmailAddress,
ls.Lifetime_Spend

FROM TEST_MasterContact m

INNER JOIN
(SELECT 
  Customer_ID__c,
  SUM(Total_Paid__c) AS Lifetime_Spend
  FROM TEST_Booking
  GROUP BY Customer_ID__c
) ls
ON m.SubscriberKey = ls.Customer_ID__c
