SELECT
m.SubscriberKey,
m.EmailAddress,
ex.Expert_Tier_Bookings,
el.Elite_Tier_Bookings

FROM MasterContact m

LEFT JOIN
(SELECT
  Customer_ID__c,
  COUNT(Category__c) AS Expert_Tier_Bookings
  FROM booking__c
  WHERE Booking_Tier__c = 'Expert'
  GROUP BY Customer_ID__c) ex
ON ex.Customer_ID__c = m.SubscriberKey

LEFT JOIN
(SELECT
  Customer_ID__c,
  COUNT(Category__c) AS Elite_Tier_Bookings
  FROM booking__c
  WHERE Booking_Tier__c = 'Elite'
  GROUP BY Customer_ID__c) el
ON el.Customer_ID__c = m.SubscriberKey
