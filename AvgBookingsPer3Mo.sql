SELECT
SubscriberKey,
EmailAddress,
ROUND(
      (
        (
          CAST(Total_Bookings__c AS float)
          /
          CAST((DATEDIFF(mm,FirstBookingDate,LastBookingDate)+1) AS float)
        )*3
      )
  ,1) AS AvgBookingsPer3Mo

FROM TEST_MasterContact m
WHERE
Total_Bookings__c IS NOT NULL
AND Total_Bookings__c > 0
AND LastBookingDate IS NOT NULL
AND FirstBookingDate IS NOT NULL