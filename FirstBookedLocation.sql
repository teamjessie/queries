SELECT m.SubscriberKey, 
m.EmailAddress,
b.area__c as FirstBookedLocation

FROM TEST_MasterContact m

INNER JOIN (
 SELECT Customer_ID__c, MIN(Booking_Start_UTC__c) as EarliestBookingDateTime
 FROM TEST_Booking
 GROUP BY Customer_ID__c
) as fd
ON fd.Customer_ID__c = m.SubscriberKey

INNER JOIN (
 SELECT MIN(Booking_Id__c) as BookingId, Customer_ID__c, Booking_Start_UTC__c
 FROM TEST_Booking
 GROUP BY Customer_ID__c, Booking_Start_UTC__c
) as fb
ON fb.Customer_ID__c = fd.Customer_ID__c
AND fb.Booking_Start_UTC__c = fd.EarliestBookingDateTime

INNER JOIN TEST_Booking b
ON b.Booking_Id__c = fb.BookingId