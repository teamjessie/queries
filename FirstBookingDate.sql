SELECT
m.SubscriberKey,
m.EmailAddress,
b.FirstBookingDate

FROM TEST_MasterContact m
INNER JOIN
(SELECT Customer_ID__c, MIN(Booking_Start_UTC__c) AS FirstBookingDate
FROM TEST_Booking
GROUP BY Customer_ID__c) b
ON m.SubscriberKey = b.Customer_ID__c