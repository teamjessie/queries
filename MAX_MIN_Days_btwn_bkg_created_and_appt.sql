SELECT
m.SubscriberKey,
m.EmailAddress,
b.MaxDays AS Max_Days_btwn_bkg_created_and_appt,
d.MinDays AS Min_Days_btwn_bkg_created_and_appt

FROM MasterContact m

LEFT JOIN

  (SELECT
  a.Customer_ID__c,
  MAX(a.DaysBtwn) AS MaxDays
  FROM
    (SELECT
    Customer_ID__c,
    DATEDIFF(dd,Booking_Created_At__c,Booking_Start_UTC__c) AS DaysBtwn
    FROM booking__c
    WHERE Booking_Created_At__c IS NOT NULL
    AND Booking_Start_UTC__c IS NOT NULL) a
GROUP BY a.Customer_ID__c) b
ON m.SubscriberKey = b.Customer_ID__c

LEFT JOIN
  (SELECT
  c.Customer_ID__c,
  MIN(c.DaysBtwn) AS MinDays
  FROM
    (SELECT
    Customer_ID__c,
    DATEDIFF(dd,Booking_Created_At__c,Booking_Start_UTC__c) AS DaysBtwn
    FROM booking__c
    WHERE Booking_Created_At__c IS NOT NULL
    AND Booking_Start_UTC__c IS NOT NULL) c
GROUP BY c.Customer_ID__c) d
ON m.SubscriberKey = d.Customer_ID__c
