SELECT
m.SubscriberKey,
m.EmailAddress,
b.AvgDays AS Avg_Days_btwn_bkg_created_and_appt

FROM MasterContact m

INNER JOIN

  (SELECT
  a.Customer_ID__c,
  AVG(a.DaysBtwn) AS AvgDays
  FROM
    (SELECT
    Customer_ID__c,
    DATEDIFF(dd,Booking_Created_At__c,Booking_Start_UTC__c) AS DaysBtwn
    FROM booking__c
    WHERE Booking_Created_At__c IS NOT NULL
    AND Booking_Start_UTC__c IS NOT NULL) a
GROUP BY a.Customer_ID__c) b

ON m.SubscriberKey = b.Customer_ID__c