SELECT
m.SubscriberKey,
m.EmailAddress,
ma.BookingsByMobileApp,
wb.BookingsByWeb,
ad.BookingsByAdmin

FROM MasterContact m

LEFT JOIN
(SELECT
  Customer_ID__c,
  COUNT(Category__c) AS BookingsByMobileApp
  FROM booking__c
  WHERE Booking_Method__c = 'Mobile App'
  GROUP BY Customer_ID__c) ma
ON ma.Customer_ID__c = m.SubscriberKey

LEFT JOIN
(SELECT
  Customer_ID__c,
  COUNT(Category__c) AS BookingsByWeb
  FROM booking__c
  WHERE Booking_Method__c = 'Web'
  GROUP BY Customer_ID__c) wb
ON wb.Customer_ID__c = m.SubscriberKey

LEFT JOIN
(SELECT
  Customer_ID__c,
  COUNT(Category__c) AS BookingsByAdmin
  FROM booking__c
  WHERE Booking_Method__c = 'Admin'
  GROUP BY Customer_ID__c) ad
ON ad.Customer_ID__c = m.SubscriberKey