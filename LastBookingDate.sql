SELECT
m.SubscriberKey,
m.EmailAddress,
b.LastBookingDate

FROM TEST_MasterContact m
INNER JOIN
(SELECT Customer_ID__c, MAX(Booking_Start_UTC__c) AS LastBookingDate
FROM TEST_Booking
GROUP BY Customer_ID__c) b
ON m.SubscriberKey = b.Customer_ID__c