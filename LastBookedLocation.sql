SELECT
m.SubscriberKey,
m.EmailAddress,
b.area__c as LastBookedLocation

FROM TEST_MasterContact m

INNER JOIN (
 SELECT Customer_ID__c, MAX(Booking_Start_UTC__c) as LatestBookingDateTime
 FROM TEST_Booking
 GROUP BY Customer_ID__c
) as ld
ON ld.Customer_ID__c = m.SubscriberKey

INNER JOIN (
 SELECT MAX(Booking_Id__c) as BookingId, Customer_ID__c, Booking_Start_UTC__c
 FROM TEST_Booking
 GROUP BY Customer_ID__c, Booking_Start_UTC__c
) as lb
ON lb.Customer_ID__c = ld.Customer_ID__c
AND lb.Booking_Start_UTC__c = ld.LatestBookingDateTime

INNER JOIN TEST_Booking b
ON b.Booking_Id__c = lb.BookingId
