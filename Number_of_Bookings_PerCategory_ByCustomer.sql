SELECT
m.SubscriberKey,
m.EmailAddress,
mk.LifetimeMakeupBookings,
hr.LifetimeHairBookings,
nl.LifetimeNailBookings,
br.LifetimeBridalBookings,
iv.LifetimeIndvBookings,
gr.LifetimeGroupBookings

FROM MasterContact m

LEFT JOIN
(SELECT
  Customer_ID__c,
  COUNT(Category__c) AS LifetimeMakeupBookings
  FROM booking__c
  WHERE Category__c = 'Makeup'
  GROUP BY Customer_ID__c) mk
ON mk.Customer_ID__c = m.SubscriberKey

LEFT JOIN
(SELECT
  Customer_ID__c,
  COUNT(Category__c) AS LifetimeHairBookings
  FROM booking__c
  WHERE Category__c = 'Hair'
  GROUP BY Customer_ID__c) hr
ON hr.Customer_ID__c = m.SubscriberKey

LEFT JOIN
(SELECT
  Customer_ID__c,
  COUNT(Category__c) AS LifetimeNailBookings
  FROM booking__c
  WHERE Category__c = 'Nail'
  GROUP BY Customer_ID__c) nl
ON nl.Customer_ID__c = m.SubscriberKey

LEFT JOIN
(SELECT
  Customer_ID__c,
  COUNT(Category__c) AS LifetimeBridalBookings
  FROM booking__c
  WHERE Category__c = 'Bridal'
  GROUP BY Customer_ID__c) br
ON br.Customer_ID__c = m.SubscriberKey

LEFT JOIN
(SELECT
  Customer_ID__c,
  COUNT(Category__c) AS LifetimeIndvBookings
  FROM booking__c
  WHERE Category__c = 'Individual'
  GROUP BY Customer_ID__c) iv
ON iv.Customer_ID__c = m.SubscriberKey

LEFT JOIN
(SELECT
  Customer_ID__c,
  COUNT(Category__c) AS LifetimeGroupBookings
  FROM booking__c
  WHERE Category__c = 'Group'
  GROUP BY Customer_ID__c) gr
ON gr.Customer_ID__c = m.SubscriberKey