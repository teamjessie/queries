SELECT
SubscriberKey,
EmailAddress,
ROUND(
      (
        CAST(Total_Bookings__c AS float)
        /
        (CAST((DATEDIFF(mm,FirstBookingDate,LastBookingDate)+1)AS float)/12)
      ),1
    ) AS AvgBookingsPerYr

FROM TEST_MasterContact
WHERE
Total_Bookings__c IS NOT NULL
AND Total_Bookings__c > 0
AND LastBookingDate IS NOT NULL
AND FirstBookingDate IS NOT NULL
